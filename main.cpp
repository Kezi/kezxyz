#include <iostream>
#include <sstream>
#include <thread>
#include <regex>

#include "engine/engine.h"

float randf(){
    return rand() / (RAND_MAX/2.0) - 1.0;
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    auto app = reinterpret_cast<pointcloud_vk*>(glfwGetWindowUserPointer(window));

    if(action == GLFW_PRESS || action == GLFW_REPEAT){
        if (key == GLFW_KEY_E){

            for(int i=0;i<1'000'000;i++){
                app->points.push_back({{randf(), randf(), randf()}, {randf(), randf(), randf()}});
            }
            app->should_reload = true;
        }

        if (key == GLFW_KEY_L){

            for(int i=0;i<10'000'000;i++){
                app->points.push_back({{randf(), randf(), randf()}, {randf(), randf(), randf()}});
            }
            app->should_reload = true;
            app->camera.setPosition(glm::vec3(-5.0f, 0.0f, 0.0f));
        }

        if(key == GLFW_KEY_KP_ADD ){
            app->zoomIn();
        }
        if(key == GLFW_KEY_KP_SUBTRACT ){
            app->zoomOut();
        }


        if(key == GLFW_KEY_0){
            app->point_size += 1.0f;
        }

        if(key == GLFW_KEY_9){
            app->point_size -= 1.0f;
        }

    }

    if(action == GLFW_PRESS ){
        if(key == GLFW_KEY_W){
            app->camera.speed.x+=1.0f;
        }

        if(key == GLFW_KEY_S){
            app->camera.speed.x-=1.0f;
        }

        if(key == GLFW_KEY_A){
            app->camera.speed.z-=1.0f;
        }

        if(key == GLFW_KEY_D){
            app->camera.speed.z+=1.0f;
        }

        if(key == GLFW_KEY_SPACE){
            app->camera.speed.y+=1.0f;
        }

        if(key == GLFW_KEY_LEFT_CONTROL){
            app->camera.speed.y-=1.0f;
        }

        if(key == GLFW_KEY_LEFT_SHIFT){
            app->camera.camera_overspeed=true;
        }

    }

    if(action == GLFW_RELEASE ){
        if(key == GLFW_KEY_W){
            app->camera.speed.x-=1.0f;
        }

        if(key == GLFW_KEY_S){
            app->camera.speed.x+=1.0f;
        }

        if(key == GLFW_KEY_A){
            app->camera.speed.z+=1.0f;
        }

        if(key == GLFW_KEY_D){
            app->camera.speed.z-=1.0f;
        }

        if(key == GLFW_KEY_SPACE){
            app->camera.speed.y-=1.0f;
        }

        if(key == GLFW_KEY_LEFT_CONTROL){
            app->camera.speed.y+=1.0f;
        }

        if(key == GLFW_KEY_LEFT_SHIFT){
            app->camera.camera_overspeed=false;
        }
    }


}

float lastX = 0, lastY = 0;
bool mouse_look_enable = false;
bool was_pressed=false;

void cursor_pos_callback(GLFWwindow* window, double xpos, double ypos)
{
    if(!mouse_look_enable)
        return;

    auto app = reinterpret_cast<pointcloud_vk*>(glfwGetWindowUserPointer(window));

    if(!was_pressed){
        lastX = xpos;
        lastY = ypos;
        was_pressed = true;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos;
    lastX = xpos;
    lastY = ypos;

    float sensitivity = 0.05;
    xoffset *= sensitivity;
    yoffset *= sensitivity;

    app->camera.rotate(xoffset, yoffset);

}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS){
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        mouse_look_enable = true;
        was_pressed = false;
    }

    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE){
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        mouse_look_enable = false;
    }
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    auto app = reinterpret_cast<pointcloud_vk*>(glfwGetWindowUserPointer(window));

    if(yoffset < 0){
        app->zoomOut();
    }

    if(yoffset > 0){
        app->zoomIn();
    }
}



int main() {

    Engine engine;

    auto input_handler=[&engine](){

        std::regex regex_xyz("^[0-9.-]+ [0-9.-]+ [0-9.-]+$");
        std::regex regex_xyzrgb("^[0-9.-]+ [0-9.-]+ [0-9.-]+ [0-9.-]+ [0-9.-]+ [0-9.-]+$");

        engine.clearPoints();
        std::string line;
        while (std::getline(std::cin, line))
        {
            if ( std::regex_match ( line.begin(), line.end(), regex_xyz ) ){
                float x, y, z;
                std::stringstream(line) >> x >> y >> z;
                engine.addPoint({x, y, z});
                continue;
            }

            if ( std::regex_match ( line.begin(), line.end(), regex_xyzrgb ) ){
                float x, y, z, r, g, b;

                std::stringstream(line) >> x >> y >> z >> r >> g >> b;
                engine.addPoint({x, y, z}, {r, g, b});
                continue;
            }

            if(line == "clear"){
                engine.clearPoints();
                continue;
            }

        }
    };


    std::thread input(input_handler);



    time_t seconds = time (NULL);
    size_t frames=0;

    engine.initialize();

    glfwSetKeyCallback(engine.window, key_callback);
    glfwSetCursorPosCallback(engine.window, cursor_pos_callback);
    glfwSetMouseButtonCallback(engine.window, mouse_button_callback);
    glfwSetScrollCallback(engine.window, scroll_callback);

    while (engine.mainLoop()) {
        if(seconds != time(NULL)){
            engine.setWindowTitle("PointCloud (" + std::to_string(frames) + " fps)");
            seconds = time(NULL);
            frames=0;
        }
        frames++;
    }

    return EXIT_SUCCESS;
}

