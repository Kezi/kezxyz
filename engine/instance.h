#ifndef INSTANCE_H
#define INSTANCE_H

#include <vulkan/vulkan.hpp>
#include <GLFW/glfw3.h>
#include <stdexcept>
#include <vector>
#include <iostream>
#include <cstring>



const std::vector<const char*> validationLayers = {
    "VK_LAYER_KHRONOS_validation"
};


class instance_wrapper
{
public:
    vk::Instance instance;

    bool enableValidationLayers = true;
    VkDebugUtilsMessengerEXT debugMessenger;

    instance_wrapper();
    ~instance_wrapper();

    void createInstance();

    bool checkValidationLayerSupport();
    std::vector<const char*> getRequiredExtensions();
    void setupDebugMessenger();

    void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo);


    static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData) {
        std::cerr << "validation layer (" << messageSeverity << "): " << pCallbackData->pMessage << std::endl;

        return VK_FALSE;
    }

};

#endif // INSTANCE_H
