#include "engine.h"

Engine::Engine()
{

}

Engine::~Engine()
{
    vkDestroySurfaceKHR(instance.instance, app.physicalDevice.surface, nullptr);
    vkDestroyInstance(instance.instance, nullptr);

    glfwDestroyWindow(window);

    glfwTerminate();
}

void Engine::initialize()
{
    glfwInit();

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    window = glfwCreateWindow(WIDTH, HEIGHT, "PointCloud", nullptr, nullptr);
    app.window = window;
    glfwSetWindowUserPointer(window, &app);

    auto framebufferResizeCallback = [](GLFWwindow* window, int x, int y) {
        auto app = reinterpret_cast<pointcloud_vk*>(glfwGetWindowUserPointer(window));
        app->framebufferResized=true;
    };

    glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);

    instance.createInstance();

    if (glfwCreateWindowSurface(instance.instance, window, nullptr, &app.physicalDevice.surface) != VK_SUCCESS) {
        throw std::runtime_error("failed to create window surface!");
    }


    app.physicalDevice.pick_physical_device(instance.instance);
    app.initVulkan(instance.enableValidationLayers);

}

void Engine::clearPoints()
{
    app.points_mutex.lock();
    app.points.clear();
    app.points_mutex.unlock();

    addPoint({0.0,0.0,0.0});
}

void Engine::setWindowTitle(std::string title)
{
    glfwSetWindowTitle(window, title.c_str());
}

void Engine::addPoint(glm::vec3 position, glm::vec3 color)
{
    app.points_mutex.lock();
    app.points.push_back({position, color});
    app.should_reload = true;
    app.points_mutex.unlock();

}

int Engine::mainLoop()
{
    glfwPollEvents();
    app.drawFrame();

    return !glfwWindowShouldClose(window);
}
