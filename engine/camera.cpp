#include "camera.h"

fps_camera::fps_camera()
{

}

void fps_camera::setPosition(glm::vec3 pos)
{
    position = pos;
}

void fps_camera::rotate(float xoffset, float yoffset)
{
    yaw += xoffset;
    pitch += yoffset;

    if(pitch > 89.0f)
        pitch = 89.0f;
    if(pitch < -89.0f)
        pitch = -89.0f;
}

glm::mat4 fps_camera::get_view(float delta)
{

    float cameraSpeedMultiplier = 1.0f;

    cameraSpeedMultiplier *= delta;

    auto cameraUp=glm::vec3(0.0f, 1.0f, 0.0f);

    glm::vec3 direction;
    direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    direction.y = sin(glm::radians(pitch));
    direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    cameraFront = glm::normalize(direction);


    static glm::vec3 smooth_camera = glm::vec3(0.0f, 0.0f, 0.0f);

    smooth_camera += (speed - smooth_camera) * delta * 5.f;

    if(camera_overspeed)
        cameraSpeedMultiplier *= 2.0f;


    position += smooth_camera.x * cameraSpeedMultiplier * cameraFront;

    position +=  glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeedMultiplier * smooth_camera.z;

    position += cameraUp * smooth_camera.y * cameraSpeedMultiplier;

    return glm::lookAt(position, position + cameraFront, cameraUp);
}
