#include "physical_device.h"
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <stdint.h>
#include <vector>
#include <stdexcept>
#include <optional>
#include <set>
#include <iostream>



QueueFamilyIndices physical_device::findQueueFamilies(vk::PhysicalDevice device) {
    QueueFamilyIndices indices;

    std::vector<vk::QueueFamilyProperties> queueFamilies = device.getQueueFamilyProperties();

    int i = 0;
    for (const auto& queueFamily : queueFamilies) {
        if (queueFamily.queueFlags & vk::QueueFlagBits::eGraphics) {
            indices.graphicsFamily = i;
        }

        VkBool32 presentSupport = false;

        //check if the queue family supports presentation to a given surface
        presentSupport = device.getSurfaceSupportKHR(i,surface);

        if (presentSupport) {
            indices.presentFamily = i;
        }

        if (indices.isComplete()) {
            break;
        }

        i++;
    }

    return indices;
}

SwapChainSupportDetails physical_device::querySwapChainSupport(vk::PhysicalDevice device) {
    SwapChainSupportDetails details;

    details.capabilities = device.getSurfaceCapabilitiesKHR(surface);

    details.formats = device.getSurfaceFormatsKHR(surface);

    details.presentModes = device.getSurfacePresentModesKHR(surface);

    return details;
}





bool physical_device::checkDeviceExtensionSupport(vk::PhysicalDevice device) {
    uint32_t extensionCount;
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

    std::vector<VkExtensionProperties> availableExtensions(extensionCount);
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

    std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());

    for (const auto& extension : availableExtensions) {
        requiredExtensions.erase(extension.extensionName);
    }

    return requiredExtensions.empty();
}



void physical_device::pick_physical_device(vk::Instance instance)
{
    auto devices = instance.enumeratePhysicalDevices();

    bool found=false;

    for (const auto& device : devices) {

        std::cout << "Trovata GPU: ";
        printDeviceProps(device);

        QueueFamilyIndices indices = findQueueFamilies(device);
        if(!indices.isComplete())
            continue;

        bool extensionsSupported = checkDeviceExtensionSupport(device);
        if(!extensionsSupported)
            continue;

        SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device);
        if(swapChainSupport.formats.empty() || swapChainSupport.presentModes.empty())
            continue;

        found = true;
        physicalDevice = device;
        break;

    }

    if ( !found ) {
        throw std::runtime_error("failed to find a suitable GPU!");
    }

    indices = findQueueFamilies(physicalDevice);


    std::cout << "Uso la gpu: ";

    printDeviceProps(physicalDevice);

}


void physical_device::printDeviceProps(VkPhysicalDevice device)
{
    VkPhysicalDeviceProperties props;
    vkGetPhysicalDeviceProperties(device, &props);

    std::cout << props.deviceName << std::endl;
}


physical_device::physical_device(){

}
