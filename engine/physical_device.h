#ifndef PHYSICAL_DEVICE_H
#define PHYSICAL_DEVICE_H
#include <vulkan/vulkan.hpp>
#include <GLFW/glfw3.h>
#include <vector>
#include <optional>

struct QueueFamilyIndices {
    std::optional<uint32_t> graphicsFamily;
    std::optional<uint32_t> presentFamily;

    bool isComplete() {
        return graphicsFamily.has_value() && presentFamily.has_value();
    }
};

struct SwapChainSupportDetails {
    vk::SurfaceCapabilitiesKHR capabilities;
    std::vector<vk::SurfaceFormatKHR> formats;
    std::vector<vk::PresentModeKHR> presentModes;
};



class physical_device
{
public:
    vk::PhysicalDevice physicalDevice;
    VkSurfaceKHR surface;
    const std::vector<const char*> deviceExtensions;
    QueueFamilyIndices indices;


    physical_device();



    QueueFamilyIndices findQueueFamilies(vk::PhysicalDevice device);
    void pick_physical_device(vk::Instance instance);
    bool checkDeviceExtensionSupport(vk::PhysicalDevice device);

    SwapChainSupportDetails querySwapChainSupport(vk::PhysicalDevice device);

    void printDeviceProps(VkPhysicalDevice device);


};

#endif // PHYSICAL_DEVICE_H
