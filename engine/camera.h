#ifndef CAMERA_H
#define CAMERA_H

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <glm/gtc/matrix_transform.hpp>


class fps_camera
{
    glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, 0.0f);

    float yaw=0, pitch=0;


public:

    glm::vec3 speed = glm::vec3(0.0f, 0.0f, 0.0f);


    bool camera_overspeed=false;



    fps_camera();

    void setPosition(glm::vec3 pos);

    void rotate(float offsetx, float offsety);

    glm::mat4 get_view(float delta);
};

#endif // CAMERA_H
