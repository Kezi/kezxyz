#ifndef ENGINE_H
#define ENGINE_H

#include "./vulkan.h"
#include <glm/glm.hpp>


class Engine
{

    instance_wrapper instance;

public:
    pointcloud_vk app;
    GLFWwindow* window;

    Engine();
    ~Engine();

    void initialize();

    void clearPoints();

    void setWindowTitle(std::string title);

    void addPoint(glm::vec3 position, glm::vec3 color = {0.0, 1.0, 0.0});

    int mainLoop();
};

#endif // ENGINE_H
