# Kezxyz

## Usage 

### Build:

```bash
mkdir build
cd build
cmake ..
make
```
Tested with vulkan-hpp 1.2.143

### Run
```bash
./pointcloud < example.xyz
```

Data is expected to be passed in the standard input

### Data format:

```c
x y z //draw point
x y z r g b //draw point with color
```

### Interaction / key bindings

`WASD`: move

`Left Shift`: move faster

`Spacebar`: move up

`Left Ctrl`: move down

`+`: zoom in

`-`: zoom out

`0`: increase point size

`9`: decrease point size

`E`: spawn 1M random points

`L`: spawn 10M random points

